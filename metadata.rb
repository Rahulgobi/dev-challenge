name             'devops-challenge'
maintainer       'Rahul'
maintainer_email 'rahulgobi4@gmail.com'
license          'All rights reserved'
description      'Installs/Configures tomcat and deploys a war file in it'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
