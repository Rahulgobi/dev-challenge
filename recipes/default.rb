#
# Cookbook Name:: devops-challenge
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

#execute sudo yum install java-1.7.0-openjdk-devel
package 'java-1.7.0-openjdk-devel' do
  
end

#execute sudo groupadd tomcat
group 'tomcat'

#execute sudo useradd -M -s /bin/nologin -g tomcat -d /opt/tomcat tomcat
user 'tomcat' do
  manage_home false
  shell '/bin/nologin'
  group 'tomcat'
  home '/opt/tomcat'
end

# wget http://apache.mirrors.hoobly.com/tomcat/tomcat-8/v8.0.36//bin/apache-tomcat-8.0.36.tar.gz
remote_file 'apache-tomcat-8.0.36.tar.gz' do
  source 'http://apache.mirrors.hoobly.com/tomcat/tomcat-8/v8.0.36//bin/apache-tomcat-8.0.36.tar.gz'  
end

directory '/opt/tomcat' do
  action :create
  group 'tomcat'
end
# Not Desired State

execute 'tar xvf apache-tomcat-8*tar.gz -C /opt/tomcat --strip-components=1'

# not a desired state
execute 'chgrp -R tomcat /opt/tomcat/conf'

directory '/opt/tomcat/conf' do
  mode '0070'
end

# not a desired state
execute 'chmod g+r /opt/tomcat/conf/*'

# not a desired state
execute 'chown -R tomcat /opt/tomcat/webapps/ /opt/tomcat/work/ /opt/tomcat/temp/ /opt/tomcat/logs/'

template '/etc/systemd/system/tomcat.service' do
  source 'tomcat.service.erb' 
end

# not a desired state
execute 'systemctl daemon-reload'

service 'tomcat' do 
  action [:start, :enable]
end
# deploying WAR file 
cookbook_file '/opt/tomcat/webapps/java-artifact-chef-test.war.zip' do
  source 'java-chef-test.war'
  owner 'root'
  group 'root'
  mode '0644'
  action :create
end
#removing the unnecessary directory
execute 'mv /opt/tomcat/webapps/java-artifact-chef-test.war.zip /opt/tomcat/webapps/java-artifact-chef-test.war'  

#restarting the tomcat 
service 'tomcat' do 
  action [:start, :enable]
end

